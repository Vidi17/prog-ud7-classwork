package es.vidal.actividad6;

public class Cliente{

    private  int nif;
    private String nombrePropio;
    private String nombreCompanyia;
    private int telefonoContacto;

    public Cliente(String nombrePropio, String nombreCompanyia, int telefonoContacto, int nif) {
        this.nombrePropio = nombrePropio;
        this.nombreCompanyia = nombreCompanyia;
        this.telefonoContacto = telefonoContacto;
        this.nif = nif;
    }

    public int getNif() {
        return nif;
    }

    public void setNumeroIdentificador(int numeroIdentificador) {
        this.nif = numeroIdentificador;
    }

}
