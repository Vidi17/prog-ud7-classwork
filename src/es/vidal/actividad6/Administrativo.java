package es.vidal.actividad6;

public class Administrativo extends Empleado{

    private int despacho;
    private int numeroFax;

    public Administrativo(String nombre, String apellidos, String dni, String direccion, int telefono, int salario, int despacho, int numeroFax) {
        super(nombre, apellidos, dni, direccion, telefono, salario);
        this.despacho = despacho;
        this.numeroFax = numeroFax;
    }

    @Override
    public void incrementarSalario(){
        salario *= 1.05;
    }

    @Override
    public String toString() {
        return super.toString()+"\nPuesto en la empresa: ADMINISTRATIVO \n" +
                "Sueldo: " + salario +
                "\nDatos Personales\n" +
                "Nombre: " + nombre +
                "\nApellidos: " + apellidos +
                "\nDNI: " + dni +
                "\nDirección: " + direccion;
    }
}
