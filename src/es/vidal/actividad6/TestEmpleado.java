package es.vidal.actividad6;

public class TestEmpleado {

    public static void main(String[] args) {

        Cliente cliente1 = new Cliente("Jordi","Google",69725578, 17);
        Cliente cliente2 = new Cliente("Pepe","HBO",64721398, 22);

        Coche coche1= new Coche("3463JVC","Ford","Focus");

        Administrativo admin1 = new Administrativo("Juan","Perez Perez","21655251Q","C/ Alicante, 80",662152432, 1000,5,72);

        Vendedor vendedor1 = new Vendedor("Vendedor1","Gomez Gomez","21564451E","Avda Juanes 15",662152432,2000,662152432,"El barrio",10,coche1);

        System.out.println("\n---- Trabajadores antes de aumentar salario ----");
        System.out.println(admin1);
        System.out.println(vendedor1);

        System.out.print("\n---- Trabajadores después de aumentar salario ----");

        vendedor1.incrementarSalario();
        admin1.incrementarSalario();

        System.out.println("\n\n" + admin1);
        System.out.println(vendedor1);

        System.out.println("\n---- Vendedor después de añadir 2 clientes ----");

        vendedor1.darDeAltaCliente(cliente1);
        vendedor1.darDeAltaCliente(cliente2);
        System.out.println(vendedor1);

    }
}
