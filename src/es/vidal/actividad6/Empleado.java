package es.vidal.actividad6;

public class Empleado {

    protected String nombre;
    protected String apellidos;
    protected String dni;
    protected String direccion;
    protected int telefono;
    protected int salario;

    public Empleado(String nombre, String apellidos, String dni, String direccion, int telefono, int salario) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salario = salario;
    }

    public void incrementarSalario(){
        salario *= 1.02;
    }

    public String toString(){
        return "\nDatos Trabajador:\n" +
                "-----------------------";
    }
}
