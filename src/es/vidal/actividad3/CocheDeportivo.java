package es.vidal.actividad3;

public class CocheDeportivo extends Coche{

    private boolean descapotable;

    public CocheDeportivo(int velocidad, float carburante, String matricula){
        super(velocidad, carburante, matricula);
        descapotable = true;
    }

    @Override
    public void acelerar() {
        super.acelerar();
        carburante -= 1;
    }
}
