package es.vidal.actividad3;

public class TestCoche {
    public static void main(String[] args) {
        Coche coche1 = new Coche(10, 10, "234532I");
        CocheDeportivo coche2 = new CocheDeportivo(20, 10, "632542J");

        coche1.repostar(50);
        coche2.repostar(50);

        coche1.acelerar(10);
        coche2.acelerar();

        coche1.frenar();
        coche2.frenar();


        coche1.mostrarNivelCarburante();
        coche2.mostrarNivelCarburante();
    }
}
