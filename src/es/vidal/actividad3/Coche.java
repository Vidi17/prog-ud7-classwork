package es.vidal.actividad3;

public class Coche extends Vehiculo {

    protected float carburante;

    public Coche(int velocidad, float carburante, String matricula) {

        super(velocidad, matricula);
        this.carburante = carburante;
    }

    public void acelerar() {

        super.acelerar(10);
        carburante -= 0.5;
    }

    public void repostar(int cantidad) {

        carburante += cantidad;
    }

    public void mostrarNivelCarburante(){

        System.out.printf("El nivel de carburante del coche es %f\n", carburante);
    }
}
