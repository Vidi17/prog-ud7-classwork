package es.vidal.actividad3;

public class Vehiculo {

    private String matricula;

    private int velocidad;

    private int ruedas;

    public Vehiculo(int velocidad, String matricula) {
        this.velocidad = velocidad;
        this.ruedas = 4;
        this.matricula = matricula;
    }
    public void acelerar(int velocidad)
    {
        this.velocidad += velocidad;
    }
    public void frenar() {
        this.velocidad = 0;
    }
}
