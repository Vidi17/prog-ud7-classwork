package es.vidal.actividad5;

public class Documental extends Produccion{

    private final String investigador;

    private final String tema;

    public Documental(String titulo, int duracion, int fechaLanzamiento, Formato formato, String investigador, String tema) {
        super(titulo, duracion, fechaLanzamiento, formato);
        this.investigador = investigador;
        this.tema = tema;
    }

    @Override
    public String toString(){
        return super.toString() +
                "(Documental) investigador = " + investigador + ", tema = " + tema;
    }
    @Override
    public void mostrarDetalle(){
        System.out.print("---------------- Documental ---------------\n");
        System.out.println("Investigador: "+ investigador);
        super.mostrarDetalle();
    }
}
