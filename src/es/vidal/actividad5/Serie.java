package es.vidal.actividad5;

public class Serie extends Produccion{

    private final int numCapituls;

    private final int numTemporadas;

    public Serie(String titulo, int duracion, int fechaLanzamiento, Formato formato, int numCapituls, int numTemporadas) {
        super(titulo, duracion, fechaLanzamiento, formato);
        this.numCapituls = numCapituls;
        this.numTemporadas = numTemporadas;
    }

    public Serie(String titulo, int fechaLanzamiento, Formato formato, int numCapituls, int numTemporadas) {
        super(titulo, 2400, fechaLanzamiento, formato);
        this.numCapituls = numCapituls;
        this.numTemporadas = numTemporadas;
    }

    @Override
    public String toString(){
        return super.toString() +
                "(Serie) temporadas = " + numTemporadas + ", capítulos = " + numCapituls;
    }

    @Override
    public void mostrarDetalle(){
        System.out.print("---------------- Serie ---------------\n");
        System.out.println("Capítulos: "+ numCapituls+" - Temporadas: "+numTemporadas);
        super.mostrarDetalle();
    }
}
