package es.vidal.actividad5;

public class BatoiFlix {
    public static void main(String[] args) {
        Fecha fecha1 = new Fecha(2020);
        Fecha fecha2 = new Fecha(2021);
        Fecha fecha3 = new Fecha(2001);

        Pelicula pelicula1 = new Pelicula("News of the world", 3600, fecha1.getAnyo(), Formato.AVI, "Tom Hanks", "Carolina Betller");

        Serie serie1 = new Serie("El hacker", 39010, fecha3.getAnyo(), Formato.FLV, 3, 200);

        Documental documental1 = new Documental("Dream Songs", 4200, fecha2.getAnyo(), Formato.MPG, "Enrique Juncosa", "Movimiento Hippie");

        pelicula1.setResumen("Lorem Ipsum Lorem Ipsum Lorem Ipsum");
        serie1.setResumen("Lorem Ipsum Lorem Ipsum Lorem Ipsum");
        documental1.setResumen("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        System.out.println("PRODUCCIONES DISPONIBLES (uso toString)");

        System.out.println(pelicula1.toString());
        System.out.println(documental1.toString());
        System.out.println(serie1.toString());

        System.out.println("\nDETALLE DE LAS PRODUCCIONES (uso mostrarDetalle)\n");

        pelicula1.mostrarDetalle();
        documental1.mostrarDetalle();
        serie1.mostrarDetalle();
    }
}
