package es.vidal.actividad14;

public abstract class Electrodomestico implements Conectable{

    private double precioBase = 100.0;

    private String marca;

    private String modelo;

    private enum Color {BLANCO, NEGRO, ROJO, AZUL, GRIS}

    private Color color = Color.BLANCO;

    private enum ConsumoEnergetico {A, B, C, D, E, F}

    private ConsumoEnergetico consumoEnergetico = ConsumoEnergetico.F;

    private int peso = 5;

    private int numUnidades = 0;

    private double precioVenta = precioBase;

    private boolean estaEncendido = false;

    public Electrodomestico(String marca, String modelo) {
        this.marca = marca;
        this.modelo = modelo;
    }

    public void obtenerPrecioVenta(){
        if (consumoEnergetico == ConsumoEnergetico.A){
            precioVenta = precioBase + 100;

        }else if (consumoEnergetico == ConsumoEnergetico.B){
            precioVenta = precioBase + 80;

        }else if (consumoEnergetico == ConsumoEnergetico.C) {
            precioVenta = precioBase + 60;

        }else if (consumoEnergetico == ConsumoEnergetico.D){
            precioVenta = precioBase + 50;

        }else if (consumoEnergetico == ConsumoEnergetico.E){
            precioVenta = precioBase + 30;

        }else {
            precioVenta = precioBase + 10;

        }
    }

    @Override
    public void mostrarConectable() {
        mostrarElectrodomestico();
    }

    @Override
    public void encender() {
        estaEncendido = true;
    }

    @Override
    public void apagar() {
        estaEncendido = false;
    }

    public void mostrarElectrodomestico(){}

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta += precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta *= precioVenta;
    }

    public int getNumUnidades() {
        return numUnidades;
    }

    public boolean isEstaEncendido() {
        return estaEncendido;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public Color getColor() {
        return color;
    }

    public ConsumoEnergetico getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setNumUnidades(int numUnidades) {
        this.numUnidades = numUnidades;
    }
}
