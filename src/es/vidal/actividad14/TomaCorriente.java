package es.vidal.actividad14;

public class TomaCorriente {

    private Conectable[] listaConectables = new Conectable[10];

    public void conectar(Conectable conectable){
        for (int i = 0; i < listaConectables.length; i++) {
            if (listaConectables[i] == null){
                listaConectables[i] = conectable;
                listaConectables[i].encender();
                return;
            }
        }
    }

    public void desconectar(Conectable conectable){
        for (int i = 0; i < listaConectables.length; i++) {
            if (listaConectables[i] == conectable){
                listaConectables[i] = null;
                conectable.apagar();
                return;
            }
        }
    }

    public int obtenerNumeroTomasLibres(){
        int numeroTomasLibres = 0;
        for (int i = 0; i < listaConectables.length; i++) {
            if (listaConectables[i] == null){
                numeroTomasLibres++;
            }
        }
        return numeroTomasLibres;
    }

    public void listarConectados(){
        System.out.println("===== Listado Aparatos Conectados =====");
        for (int i = 0; i < listaConectables.length; i++) {
            if (listaConectables[i] != null){
                listaConectables[i].mostrarConectable();
            }
        }
    }
}
