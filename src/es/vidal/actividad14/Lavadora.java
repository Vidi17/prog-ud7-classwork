package es.vidal.actividad14;

public class Lavadora extends Electrodomestico{

    private double carga = 5.0;

    public Lavadora(String marca, String modelo, double carga) {
        super(marca, modelo);
        this.carga = carga;
    }

    public Lavadora(String marca, String modelo) {
        super(marca, modelo);
    }

    @Override
    public void mostrarElectrodomestico() {
        System.out.printf("[Lavadora] carga: %.1f Marca: %s, Modelo: %s, Tipo Consumo: %s, Color: %s, Precio: %.2f, " +
                "Precio de Venta: %.2f, Encendido: %b\n", carga, getMarca(), getModelo(), getConsumoEnergetico(),
                getColor(), getPrecioBase(), getPrecioVenta(), isEstaEncendido());
    }

    @Override
    public void obtenerPrecioVenta() {
        super.obtenerPrecioVenta();
        if (carga > 30){
            setPrecioVenta(50);
        }
    }
}
