package es.vidal.actividad14;

public class Television extends Electrodomestico{

    private int resolucion = 20;

    private boolean tieneSmartTV = false;

    public Television(String marca, String modelo, int resolucion, boolean tieneSmartTV) {
        super(marca, modelo);
        this.resolucion = resolucion;
        this.tieneSmartTV = tieneSmartTV;
    }

    public Television(String marca, String modelo) {
        super(marca, modelo);
    }

    @Override
    public void mostrarElectrodomestico() {
        System.out.printf("[Televisor] resolución: %d, Smart TV: %s Marca: %s, Modelo: %s, Tipo Consumo: %s, Color: %s, Precio: %.2f, " +
                        "Precio de Venta: %.2f, Encendido: %b\n", resolucion, tieneSmartTV, getMarca(), getModelo(), getConsumoEnergetico(),
                getColor(), getPrecioBase(), getPrecioVenta(), isEstaEncendido());
    }

    @Override
    public void obtenerPrecioVenta() {
        super.obtenerPrecioVenta();
        if (resolucion > 40 && tieneSmartTV){
            setPrecioVenta(1.3);
            setPrecioVenta(50);
        }else if (tieneSmartTV){
            setPrecioVenta(50);
        }else if (resolucion > 40){
            setPrecioVenta(50);
        }
    }
}
