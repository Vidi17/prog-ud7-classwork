package es.vidal.actividad14;

public class Computador implements Conectable{

    private int memoriaRam;

    private double velocidadCpu;

    private boolean estaEncendido = false;

    private int tamanyoDisco;

    private double precioBase;

    private double precioVenta;

    @Override
    public void encender() {
        estaEncendido = true;
    }

    @Override
    public void apagar() {
        estaEncendido = false;
    }

    public Computador(int memoriaRam, double velocidadCpu, int tamanyoDisco, double precioBase, double precioVenta) {
        this.memoriaRam = memoriaRam;
        this.velocidadCpu = velocidadCpu;
        this.tamanyoDisco = tamanyoDisco;
        this.precioBase = precioBase;
        this.precioVenta = precioVenta;
    }

    @Override
    public void mostrarConectable() {
        System.out.printf("[Computador] RAM: %d Velocidad CPU: %.1f, Capacidad HDD: %d, Precio Base: %.2f," +
                        " Precio Venta:" + " %.2f, Encendido: %s\n", memoriaRam, velocidadCpu, tamanyoDisco, precioBase,
                            precioVenta, estaEncendido);
    }
}
