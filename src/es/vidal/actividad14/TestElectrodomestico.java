package es.vidal.actividad14;

public class TestElectrodomestico {
    public static void main(String[] args) {
        Electrodomestico[] listaElectrodomesticos = new Electrodomestico[10];

        Electrodomestico lavadora1 = new Lavadora("Bosh", "Lo367-R");

        Electrodomestico lavadora2 = new Lavadora("Siemens", "P25-345", 200);

        Electrodomestico lavadora3 = new Lavadora("Hitachi", "WW90TA046-TE");

        Electrodomestico lavadora4 = new Lavadora("Sony", "FF0914-DOES", 400);

        Electrodomestico lavadora5 = new Lavadora("Suzuki", "NVR4503");

        lavadora1.obtenerPrecioVenta();
        lavadora2.obtenerPrecioVenta();
        lavadora3.obtenerPrecioVenta();
        lavadora4.obtenerPrecioVenta();
        lavadora5.obtenerPrecioVenta();

        Electrodomestico televisor1 = new Television("Xiaomi", "HDQ5FDS");

        Electrodomestico televisor2 = new Television("Samsung", "ÑLDK4K", 40, true);

        Electrodomestico televisor3 = new Television("Hitachi", "OPERDAS*45");

        Electrodomestico televisor4 = new Television("LG", "2345-PO", 80, false);

        Electrodomestico televisor5 = new Television("Telefunken", "WERD-342");

        televisor1.obtenerPrecioVenta();
        televisor2.obtenerPrecioVenta();
        televisor3.obtenerPrecioVenta();
        televisor4.obtenerPrecioVenta();
        televisor5.obtenerPrecioVenta();

        listaElectrodomesticos[0] = lavadora1;
        listaElectrodomesticos[1] = lavadora2;
        listaElectrodomesticos[2] = lavadora3;
        listaElectrodomesticos[3] = lavadora4;
        listaElectrodomesticos[4] = lavadora5;
        listaElectrodomesticos[5] = televisor1;
        listaElectrodomesticos[6] = televisor2;
        listaElectrodomesticos[7] = televisor3;
        listaElectrodomesticos[8] = televisor4;
        listaElectrodomesticos[9] = televisor5;

        System.out.println("----- Parte A -------");
        verListado(listaElectrodomesticos);
        System.out.println("----- Parte A -------");

    }

    public static void verListado(Electrodomestico[] listaElectrodomesticos){
        System.out.println("===== Listado Electrodomesticos =====");
        for (int i = 0; i < listaElectrodomesticos.length; i++) {
            if (listaElectrodomesticos[i] != null){
                listaElectrodomesticos[i].mostrarElectrodomestico();
            }
        }
        System.out.println("==================================");
    }
}
