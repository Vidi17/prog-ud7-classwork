package es.vidal.actividad14;

public interface Conectable {

    void encender();

    void apagar();

    void mostrarConectable();

}
