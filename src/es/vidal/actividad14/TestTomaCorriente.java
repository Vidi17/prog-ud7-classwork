package es.vidal.actividad14;

public class TestTomaCorriente {
   public static void main(String[] args) {

       Computador computador1 = new Computador(16, 3.3, 78, 100.0, 170.80);

       Computador computador2 = new Computador(24, 4.0, 349, 300.0, 359.40);

       Electrodomestico lavadora1 = new Lavadora("Bosh", "Lo367-R");

       Electrodomestico lavadora2 = new Lavadora("Siemens", "P25-345", 200);

       lavadora1.obtenerPrecioVenta();
       lavadora2.obtenerPrecioVenta();

       Electrodomestico televisor1 = new Television("Xiaomi", "HDQ5FDS");

       Electrodomestico televisor2 = new Television("Samsung", "ÑLDK4K", 40, true);

       televisor1.obtenerPrecioVenta();
       televisor2.obtenerPrecioVenta();

       TomaCorriente tomaCorriente1 = new TomaCorriente();

       tomaCorriente1.conectar(computador1);
       tomaCorriente1.conectar(computador2);
       tomaCorriente1.conectar(lavadora1);
       tomaCorriente1.conectar(lavadora2);
       tomaCorriente1.conectar(televisor1);
       tomaCorriente1.conectar(televisor2);

       System.out.println("----- Parte B -------");
       tomaCorriente1.listarConectados();
       System.out.println("==================================");
       tomaCorriente1.listarConectados();
       System.out.println("--------------------");
   }
}
