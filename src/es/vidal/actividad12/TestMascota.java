package es.vidal.actividad12;

public class TestMascota {
    public static void main(String[] args) {

        Persona persona = new Persona("Jordi","Vidal",18,false);
        Perro perro = new Perro(Animal.Tamanyo.PEQUENYO,"Alcoy");
        Gato gato = new Gato(Animal.Tamanyo.PEQUENYO,"Spain");
        RoboPerro roboPerro = new RoboPerro();

        System.out.println("------ Test Mascota ------");
        persona.interactuar(gato);
        persona.interactuar(perro);
        persona.interactuar(roboPerro);

    }
}
