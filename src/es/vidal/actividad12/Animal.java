package es.vidal.actividad12;

abstract public class Animal {

    protected boolean vacunado;

    enum Comida{HERBIVORO, CARNIVORO, OMNIVORO}

    public Comida comida;

    protected int hambre;

    enum Tamanyo{GRANDE, PEQUENYO, MEDIANO}

    public Tamanyo tamanyo;

    protected String localizacion;

    public abstract void emitirSonido();

    public void comer(){
        hambre = 0;
    }

    public void vacunar(){
        vacunado = true;
        System.out.println("Vacunando...");
    }

    public String toString(){
        return "";
    }
}


