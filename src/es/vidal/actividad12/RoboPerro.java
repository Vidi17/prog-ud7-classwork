package es.vidal.actividad12;

public class RoboPerro implements Pet{
    @Override
    public void beFriendly() {
        System.out.println("Moviendo la Cabeza");
    }

    @Override
    public void play() {
        System.out.println("Siguiendo una línea");
    }
}
