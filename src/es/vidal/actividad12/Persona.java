package es.vidal.actividad12;

public class Persona {

    protected String nombre;
    protected String apellidos;
    protected int edad;
    protected boolean casado;

    public Persona(String nombre, String apellidos, int edad, boolean casado) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.casado = casado;
    }
    public Persona(String nombre, String apellidos, int edad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.casado = false;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public boolean isCasado() {
        return casado;
    }

    public void saluda(){
        String estado = (casado)? "estoy casado":"no estoy casado";
        System.out.printf("Hola soy %s, mi edad es %d y estoy %s%n",nombre,edad,estado);
    }

    public void interactuar(Pet mascota){
        if (mascota instanceof Gato){
            System.out.println("Hola gatito");
        }else if (mascota instanceof Perro){
            System.out.println("Hola Perrito");
        }else if (mascota instanceof RoboPerro){
            System.out.println("Encendiendo el Robot");
        }
        mascota.beFriendly();
        mascota.play();
        System.out.println();
    }
}
