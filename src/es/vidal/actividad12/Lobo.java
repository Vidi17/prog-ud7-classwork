package es.vidal.actividad12;

public class Lobo extends Animal {

    public Lobo(Tamanyo tamanyo,String localizacion) {

        this.vacunado = false;
        this.comida = Comida.CARNIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("AUUUUUUUHHHH!!!!!!!!!");
    }

    public String toString() {
        return "Lobo: tamaño = " + tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }
}
