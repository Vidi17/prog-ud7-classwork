package es.vidal.actividad12;

public class TestAnimals {
    public static void main(String[] args) {
        Veterinario veterinario =new Veterinario();
        Animal leon1 = new Leon(Animal.Tamanyo.GRANDE,"La Sabana");
        Animal lobo1 = new Lobo(Animal.Tamanyo.GRANDE,"La Sabana");
        Animal lobo2 = new Lobo(Animal.Tamanyo.MEDIANO,"La Serreta");
        Animal perro = new Perro(Animal.Tamanyo.PEQUENYO,"Alcoy");
        Animal gato1 = new Gato(Animal.Tamanyo.PEQUENYO,"Spain");
        Animal gato2 = new Gato(Animal.Tamanyo.PEQUENYO,"Spain");
        Animal hipopotamo= new Hipopotamo(Animal.Tamanyo.GRANDE,"La Sabana");
        Animal tigre1= new Tigre(Animal.Tamanyo.GRANDE,"Africa");
        Animal tigre2= new Tigre(Animal.Tamanyo.GRANDE,"Africa");

        System.out.println(leon1);
        System.out.println(lobo1);
        System.out.println(lobo2);
        System.out.println(perro);
        System.out.println(gato1);
        System.out.println(gato2);
        System.out.println(hipopotamo);
        System.out.println(tigre1);
        System.out.println(tigre2);

        veterinario.vacunar(leon1);
        veterinario.vacunar(lobo1);
        veterinario.vacunar(lobo2);
        veterinario.vacunar(perro);
        veterinario.vacunar(gato1);
        veterinario.vacunar(gato2);
        veterinario.vacunar(hipopotamo);
        veterinario.vacunar(tigre1);
        veterinario.vacunar(tigre2);

        System.out.println(leon1);
        System.out.println(lobo1);
        System.out.println(lobo2);
        System.out.println(perro);
        System.out.println(gato1);
        System.out.println(gato2);
        System.out.println(hipopotamo);
        System.out.println(tigre1);
        System.out.println(tigre2);
    }
}
