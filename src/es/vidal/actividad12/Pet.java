package es.vidal.actividad12;

public interface Pet {

    void beFriendly();

    void play();

}
