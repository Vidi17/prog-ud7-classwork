package es.vidal.actividad12;

public class Veterinario {

    public void alimentar(Animal animal){
        animal.comer();
    }

    public void vacunar(Animal animal){
        if (animal instanceof Leon){
            alimentar(animal);
            animal.vacunar();
            animal.emitirSonido();
        }else if (animal instanceof Tigre){
            alimentar(animal);
            animal.vacunar();
            animal.emitirSonido();
        }else {
            animal.vacunar();
            animal.emitirSonido();
        }
    }
}
