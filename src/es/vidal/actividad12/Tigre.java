package es.vidal.actividad12;

public class Tigre extends Animal {

    public Tigre(Tamanyo tamanyo,String localizacion) {
        this.vacunado = false;
        this.comida = Comida.CARNIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("OAUGGGGGGG....!");
    }

    public String toString() {
        return "Tigre: tamaño = "+ tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }
}