package es.vidal.actividad12;

public class Gato extends Animal implements Pet {

    public Gato(Tamanyo tamanyo,String localizacion) {

        this.vacunado = false;
        this.comida = Comida.CARNIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("Miauuuu!!!!!!!!!");
    }

    @Override
    public String toString() {
        return "Gato: tamaño = " + tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }

    @Override
    public void beFriendly() {
        System.out.println("Ronroneo");
    }

    @Override
    public void play() {
        System.out.println("Jugando con el ovillo de lana");
    }
}
