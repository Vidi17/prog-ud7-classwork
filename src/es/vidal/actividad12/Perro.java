package es.vidal.actividad12;

public class Perro extends Animal implements Pet {

    public Perro(Tamanyo tamanyo,String localizacion) {

        this.vacunado = false;
        this.comida = Comida.OMNIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("GUAU!!!");
    }

    public String toString() {
        return "Perro: tamaño = " + tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }

    @Override
    public void beFriendly() {
        System.out.println("Moviendo la cola");

    }

    @Override
    public void play() {
        System.out.println("Jugando con el frisbee");

    }
}
