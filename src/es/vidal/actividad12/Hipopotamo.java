package es.vidal.actividad12;

public class Hipopotamo extends Animal {

    public Hipopotamo(Tamanyo tamanyo,String localizacion) {

        this.vacunado = false;
        this.comida = Comida.HERBIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("HIPPPP!!!!!!!!!");
    }

    @Override
    public String toString() {
        return "Hipopotamo: tamaño = " + tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }
}
