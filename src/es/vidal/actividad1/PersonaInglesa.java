package es.vidal.actividad1;

public class PersonaInglesa extends Persona {

    public PersonaInglesa(String nombre, String apellidos, int edad, boolean casado) {
        super(nombre, apellidos, edad, casado);
    }

    @Override
    public void saluda() {
        String estado = (casado) ? "married" : "not married";
        System.out.printf("Hello i %s, my age is %d and i %s%n", nombre, edad, estado);
    }
}
