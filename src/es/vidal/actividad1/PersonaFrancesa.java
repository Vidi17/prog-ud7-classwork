package es.vidal.actividad1;

public class PersonaFrancesa extends Persona {

    public PersonaFrancesa(String nombre, String apellidos, int edad, boolean casado) {
        super(nombre, apellidos, edad, casado);
    }

    @Override
    public void saluda() {
        String estado = (casado) ? "married" : "not married";
        System.out.printf("Bonjour je %s, mon age est %d et je %s%n", nombre, edad, estado);
    }
}
