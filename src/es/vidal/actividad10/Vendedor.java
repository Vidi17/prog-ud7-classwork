package es.vidal.actividad10;

public class Vendedor extends Empleado {

    private final int NUMERO_MAXIMO_CLIENTES = 10;

    private int telefonoMovil;
    private String areaVenta;
    private Cliente[] listaClientes = new Cliente[NUMERO_MAXIMO_CLIENTES];
    private int totalClientes;
    private double porcentajeComisiones;
    private boolean estadoCliente;
    private Coche cocheVendedor;

    public Vendedor(String nombre, String apellidos, String dni, String direccion, int telefono, double salario, int telefonoMovil, String areaVenta, double porcentajeComisiones, Coche cocheVendedor) {
        super(nombre, apellidos, dni, direccion, telefono, salario);
        this.telefonoMovil = telefonoMovil;
        this.areaVenta = areaVenta;
        this.porcentajeComisiones = porcentajeComisiones;
        this.cocheVendedor = cocheVendedor;
        totalClientes = 0;
    }

    @Override
    public void incrementarSalario(){
        salario *= 1.1;
    }

    public void cambiarCoche(String matricula, String marca, String modelo){
        cocheVendedor = new Coche(matricula, marca, modelo);
    }

    public void darDeAltaCliente(Cliente cliente){
        for (int i = 0; i < listaClientes.length; i++) {
            if (listaClientes[i] == null){
                listaClientes[i] = cliente;
                totalClientes++;
                return;
            }
        }
    }

    public void darDeBajaCliente(Cliente cliente){
        for (int i = 0; i < listaClientes.length; i++) {
            if (listaClientes[i].getNif() == cliente.getNif()){
                listaClientes[i] = null;
                totalClientes--;
                return;
            }
        }
    }


    @Override
    public String toString() {
        String salarioDecimal = String.format( "%.2f", (salario));
        return super.toString()+"Puesto en la empresa: VENDEDOR" +
                ", Nombre: " + nombre +
                ", Apellidos: " + apellidos +
                ",DNI:\n" + dni +
                ", Sueldo: " + salarioDecimal +
                ", Numero Clientes: " + totalClientes;
    }

}
