package es.vidal.actividad10;

public class Empresa {

    private final int MAX_EMPLEADOS = 20;

    private Empleado[] listaEmpleados = new Empleado[MAX_EMPLEADOS];

    public void anyadirEmpleado(Empleado empleado, String dni){
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] == null && !comprobacionDni(dni)){
                listaEmpleados[i] = empleado;
                return;
            }
        }
    }

    public boolean comprobacionDni(String dni){
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] != null && listaEmpleados[i].getDni().equals(dni)){
                return true;
            }
        }
        return false;
    }

    public void borrarEmpleado(String dni){
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i].getDni().equals(dni)){
                listaEmpleados[i] = null;
            }
        }
    }

    public String mostrarEmpleado(Empleado empleado){
        return empleado.toString();
    }

    public void mostrarListaEmpleados(){
        System.out.println("------ Todos Los empleados --------");
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] != null) {
                System.out.printf("%d) %s\n", i, mostrarEmpleado(listaEmpleados[i]));
            }
        }
        System.out.println("------------------------\n");
    }

    public void listarVendedores(){
        System.out.println("------ Vendedores --------");
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] instanceof Vendedor){
                System.out.printf("%d) %s\n", i, mostrarEmpleado(listaEmpleados[i]));
            }
        }
        System.out.println("------------------------");
    }

    public void listarAdministrativos(){
        System.out.println("------ Administrativos --------");
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] instanceof Administrativo){
                System.out.printf("%d) %s\n", i, mostrarEmpleado(listaEmpleados[i]));
            }
        }
        System.out.println("------------------------");
    }

    public double totalMensual(){
        int totalMensual = 0;
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] != null) {
                totalMensual += listaEmpleados[i].getSalario();
            }
        }
        return totalMensual;
    }

    public void aumentoSalarioGeneral(){
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] != null){
                listaEmpleados[i].incrementarSalario();
            }
        }
    }
}
