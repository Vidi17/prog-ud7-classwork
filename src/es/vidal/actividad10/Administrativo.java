package es.vidal.actividad10;

public class Administrativo extends Empleado {

    private int despacho;
    private int numeroFax;

    public Administrativo(String nombre, String apellidos, String dni, String direccion, int telefono, double salario, int despacho, int numeroFax) {
        super(nombre, apellidos, dni, direccion, telefono, salario);
        this.despacho = despacho;
        this.numeroFax = numeroFax;
    }

    @Override
    public void incrementarSalario(){
        salario *= 1.05;
    }

    @Override
    public String toString() {
        String salarioDecimal = String.format( "%.2f", (salario));
        return super.toString()+"Puesto en la empresa: ADMINISTRATIVO" +
                ", Nombre: " + nombre +
                ", Apellidos: " + apellidos +
                ",\nDNI: " + dni +
                ", Sueldo: " + salarioDecimal +
                ", Oficina: " + despacho +
                ", Fax: +" + numeroFax;
    }
}
