package es.vidal.actividad10;

public abstract class Empleado {

    protected String nombre;
    protected String apellidos;
    protected String dni;
    protected String direccion;
    protected int telefono;
    protected double salario;

    public Empleado(String nombre, String apellidos, String dni, String direccion, int telefono, double salario) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public String getDni() {
        return dni;
    }

    public void incrementarSalario(){
        salario *= 1.02;
    }

    public String toString(){
        return "";
    }
}
