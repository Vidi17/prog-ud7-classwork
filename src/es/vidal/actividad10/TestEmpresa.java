package es.vidal.actividad10;

public class TestEmpresa {
    public static void main(String[] args) {

        Empresa empresa1 = new Empresa();

        Empleado admin1 = new Administrativo("Juan","Perez Perez","21655251Q","C/ Alicante, 80",662152432, 1007.21,5,32456633);
        Empleado admin2 = new Administrativo("Carlos","Moncho Perez","61235231J","C/ Pepos, 35",692452731, 2300.79,9,72543345);
        Empleado admin3 = new Administrativo("Jordi","Vidal Cerdà","21805945J","C/ Santos, 12",687564234, 7903.82,12,2346542);
        Empleado admin4 = new Administrativo("Pepe","Martinez Juan","31255689X","C/ Alcoy, 92",634564167, 5940.43,76,23456654);
        Empleado admin5 = new Administrativo("Sami","Fuentes Mollà","22644571L","C/ Perro, 45",634785092, 1456.80,8,890759034);

        Coche coche1= new Coche("3463JVC","Ford","Focus");
        Coche coche2= new Coche("4567IOS","Ford","Fiesta");
        Coche coche3= new Coche("6853PSO","Nissan","Juke");
        Coche coche4= new Coche("9845SKI","Honda","Civic");
        Coche coche5= new Coche("3456SLI","Skoda","Fabia");

        Empleado vendedor1 = new Vendedor("Vendedor1","Gomez Gomez","21564451E","Avda Juanes 15",191693709,2000,176993393,"El barrio",10,coche1);
        Empleado vendedor2 = new Vendedor("Vendedor2","Gomez Perez","21564561J","Avda Panes 17",536126709,4205.09,199345901,"La calle",20,coche2);
        Empleado vendedor3 = new Vendedor("Vendedor3","Perez Gomez","21564671L","Avda Gato 79",544130635,2300.34,960931984,"El bajoMundo",14,coche3);
        Empleado vendedor4 = new Vendedor("Vendedor4","Silva Gomez","21564781K","Avda Linares 23",568637008,7000.35,152651981,"La plebe",25,coche4);
        Empleado vendedor5 = new Vendedor("Vendedor5","Gomez Juan","21564489D","Avda Edward 11",443056736,5864.98,222167768,"El matadero",16,coche5);

        empresa1.anyadirEmpleado(admin1, admin1.getDni());
        empresa1.anyadirEmpleado(admin2, admin2.getDni());
        empresa1.anyadirEmpleado(admin3, admin3.getDni());
        empresa1.anyadirEmpleado(admin4, admin4.getDni());
        empresa1.anyadirEmpleado(admin5, admin5.getDni());

        empresa1.anyadirEmpleado(vendedor1, vendedor1.getDni());
        empresa1.anyadirEmpleado(vendedor2, vendedor2.getDni());
        empresa1.anyadirEmpleado(vendedor3, vendedor3.getDni());
        empresa1.anyadirEmpleado(vendedor4, vendedor4.getDni());
        empresa1.anyadirEmpleado(vendedor5, vendedor5.getDni());

        empresa1.mostrarListaEmpleados();

        System.out.printf("El coste mensual en salarios es %.2f €\n\n", empresa1.totalMensual());

        System.out.println("------ Aumento salario de empleados ------\n");

        empresa1.aumentoSalarioGeneral();
        empresa1.mostrarListaEmpleados();

        System.out.printf("El coste mensual en salarios es %.2f €\n\n", empresa1.totalMensual());

        System.out.println("------ Listados con filtros ------\n");

        empresa1.listarAdministrativos();
        empresa1.listarVendedores();

    }
}
