package es.vidal.actividad8;

import es.vidal.actividad8.Animal;

public class Gato extends Animal {

    public Gato(Tamanyo tamanyo,String localizacion) {

        this.vacunado = false;
        this.comida = Comida.CARNIVORO;
        this.hambre = 8;
        this.tamanyo = tamanyo;
        this.localizacion = localizacion;
    }

    @Override
    public void emitirSonido() {
        System.out.println("Miauuuu!!!!!!!!!");
    }

    @Override
    public String toString() {
        return "Gato: tamaño = " + tamanyo + ", Nivel de hambre = " + hambre + ", vacunado = " + vacunado + " vive en = " + localizacion;
    }
}
