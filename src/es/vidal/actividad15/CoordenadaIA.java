package es.vidal.actividad15;

import java.util.Random;

public class CoordenadaIA extends Coordenada{

    public static Random aleatorio = new Random();

    public CoordenadaIA(int x, int y) {
        setX(x);
        setY(y);
    }

    public CoordenadaIA() { }

    @Override
    public void recoger() {
        do {
            setX(aleatorio.nextInt(3 ));
            setY(aleatorio.nextInt(3 ));

            if (getX() > 2 || getX() < 0 || getY() > 2 || getY() < 0){
                System.out.println("Error! Introduce una coordenada Válida");
            }
        }while (getX() > 2 || getX() < 0 || getY() < 0 || getY() > 2);
    }


}
