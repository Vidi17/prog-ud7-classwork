package es.vidal.actividad15;

import java.util.Scanner;

public class Menu {

    public static Scanner teclado = new Scanner(System.in);

    private final int JUGADOR_VS_ORDENADOR = 1;
    private final int JUGADOR_1_VS_JUGADOR_2 = 2;
    private final int ORDENADOR_VS_ORDENADOR = 3;

    private final Icono packIconos = new Icono();

    private final Partida partida = new Partida();

    public void mostrar(){
        System.out.println("Bienvenido al 3 en Raya\n" +
                "================================");
        partida.insertarJugadores(seleccionPartida());
        partida.jugar();
    }

    public Jugador[] seleccionPartida(){
        System.out.printf("""
                %d - Jugador vs Ordenador
                %d - Jugador 1 vs Jugador 2
                %d - Ordenador vs Ordenador
                4 - Salir

                Seleccione una opción:\s""", JUGADOR_VS_ORDENADOR, JUGADOR_1_VS_JUGADOR_2, ORDENADOR_VS_ORDENADOR);
        int seleccionTipo;

        do {
            seleccionTipo = conseguirEntero();
            if (seleccionTipo < 1 || seleccionTipo > 4){
                System.out.print("Por favor seleccione una opción válida: ");
            }
        }while (seleccionTipo < 1 || seleccionTipo > 4);

        System.out.println();

        return crearListaJugadores(seleccionTipo);
    }

    public int conseguirEntero(){
        boolean isEntero;
        do {
            if (!teclado.hasNextInt()) {
                System.out.print("Debe introducir un entero: ");
                teclado.next();
                isEntero = false;
            }else {
                isEntero = true;
            }
        }while (!isEntero);
        return teclado.nextInt();
    }

    public Jugador[] crearListaJugadores(int seleccionTipo){
        return switch (seleccionTipo) {
            case JUGADOR_VS_ORDENADOR -> new Jugador[]{crearJugadorHumano(1), crearJugadorMaquina(2)};
            case JUGADOR_1_VS_JUGADOR_2 -> new Jugador[]{crearJugadorHumano(1), crearJugadorHumano(2)};
            case ORDENADOR_VS_ORDENADOR -> new Jugador[]{crearJugadorMaquina(1), crearJugadorMaquina(2)};
            default -> new Jugador[2];
        };
    }

    public Jugador crearJugadorHumano(int numeroJugador){
        if (numeroJugador == 1){
            return new JugadorHumano(Casilla.FICHA_0, packIconos);
        }else {
            return new JugadorHumano(Casilla.FICHA_1, packIconos);
        }
    }

    public Jugador crearJugadorMaquina(int numeroJugador){
        if (numeroJugador == 1){
            return new JugadorMaquina(Casilla.FICHA_0, packIconos);
        }else {
            return new JugadorMaquina(Casilla.FICHA_1, packIconos);
        }
    }
}
