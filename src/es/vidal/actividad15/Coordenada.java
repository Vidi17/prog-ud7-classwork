package es.vidal.actividad15;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Coordenada {

    public static Scanner teclado = new Scanner(System.in);

    private int y;

    private int x;

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void recoger(){
        do {
            x = getFila();
            y = getColumna();

            if (x > 3 || x < 1 || y < 1 || y > 3){
                System.out.println("Error! Introduce una coordenada Válida");
            }
        }while (x > 3 || x < 1 || y < 1 || y > 3);
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    private int getFila(){
        int fila = 0;
        boolean continua;
        do {
            try {
                continua = false;
                System.out.print("\nIntroduce fila [1-3]: ");
                fila = teclado.nextInt();
            }catch (InputMismatchException ex){
                System.out.println("Debe ingresar obligatoriamente un número entero");
                teclado.next();
                continua = true;
            }
        }while (continua);

        return fila;
    }

    private int getColumna(){
        int columna = 0;
        boolean continua;
        do {
            try {
                continua = false;
                System.out.print("\nIntroduce columna [1-3]: ");
                columna = teclado.nextInt();
            }catch (InputMismatchException ex){
                System.out.println("Debe ingresar obligatoriamente un número entero");
                teclado.next();
                continua = true;
            }
        }while (continua);

        return columna;
    }
}
