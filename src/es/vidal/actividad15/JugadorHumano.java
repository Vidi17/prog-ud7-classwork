package es.vidal.actividad15;

public class JugadorHumano extends Jugador{

    public JugadorHumano(Casilla fichaJugador, Icono packIconos) {
        super(fichaJugador, packIconos);
    }

    @Override
    public void ponerFicha(Tablero tablero) {
        Coordenada coordenada = new Coordenada();
        System.out.printf("\nTira el jugador con %s\n", getIcono().obtenerSimbolo(getFichaJugador()));
        do {
            coordenada.recoger();
            if(tablero.isOcupada(coordenada)){
                System.out.println("La casilla está ocupada, por favor vuelva a introducir las coordenadas");
            }
        }while (tablero.isOcupada(coordenada));
        tablero.ponerFicha(coordenada, getFichaJugador());
    }

    @Override
    public void cantaVictoria() {

        System.out.println("\nGana la partida el jugador con las fichas " +
                getIcono().obtenerSimboloJugador(getFichaJugador()));

    }
}
