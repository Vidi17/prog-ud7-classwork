package es.vidal.actividad15;

public class Tablero {

    public static final int NUMERO_TOTAL_CASILLAS = 9;

    private final Casilla[][] casillas = new Casilla[3][3];

    private final Icono packIconos;

    public Tablero(Icono packIconos) {
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                casillas[i][j] = Casilla.VACIA;

            }
        }
        this.packIconos = packIconos;
    }

    public Casilla[][] getCasillas() {
        return casillas;
    }

    public void mostrar() {
        for (Casilla[] casilla : casillas) {
            for (int j = 0; j < casillas.length; j++) {
                System.out.printf("%s ", (packIconos.obtenerSimbolo(casilla[j])));
            }
            System.out.println();
        }
    }

    public boolean hayTresEnRaya() {
        for (int i = 0; i < casillas.length; i++) {
            if (isTresEnRayaFila(i)) {
                return true;
            }else if (isTresEnRayaColumna(i)) {
                return true;
            }
        }
        if (isTresEnRayaDiagonalMayor()) {
            return true;
        }else{
            return isTresEnRayaSubdiagonal();
        }
    }

    public boolean isTresEnRayaDiagonalMayor(){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[i][i] == Casilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[i][i] == Casilla.FICHA_1){
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean isTresEnRayaSubdiagonal(){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        int j = casillas.length - 1;
        for (Casilla[] casilla : casillas) {
            if (casilla[j] == Casilla.FICHA_0) {
                contadorFichas0++;
            } else if (casilla[j] == Casilla.FICHA_1) {
                contadorFichasX++;
            }
            j--;
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean isTresEnRayaColumna(int columna){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (Casilla[] casilla : casillas) {
            if (casilla[columna] == Casilla.FICHA_0) {
                contadorFichas0++;
            } else if (casilla[columna] == Casilla.FICHA_1) {
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean isTresEnRayaFila(int fila){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[fila][i] == Casilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[fila][i] == Casilla.FICHA_1){
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean isTresEnRaya(int contadorFichas0, int contadorFichasX){
        return contadorFichas0 == 3 || contadorFichasX == 3;
    }

    public boolean isOcupada(Coordenada coordenada) {
        if (coordenada instanceof CoordenadaIA){
            return casillas[coordenada.getX()][coordenada.getY()] == Casilla.FICHA_0 || casillas[coordenada.getX()][coordenada.getY()] == Casilla.FICHA_1;
        }else {
            return casillas[coordenada.getX() - 1][coordenada.getY() - 1] == Casilla.FICHA_0 || casillas[coordenada.getX() - 1][coordenada.getY() - 1] == Casilla.FICHA_1;
        }
    }

    public boolean isOcupada(int x, int y) {
        return casillas[x][y] == Casilla.FICHA_0 || casillas[x][y] == Casilla.FICHA_1;
    }

    public void ponerFicha(Coordenada coordenada, Casilla ficha){
        if (coordenada instanceof CoordenadaIA){
            casillas[coordenada.getX()][coordenada.getY()] = ficha;
        }else {
            casillas[coordenada.getX() - 1][coordenada.getY() - 1] = ficha;
        }
    }

    public boolean estaLleno(){
        int contador = 0;
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                if (isOcupada(i,j)){
                    contador++;
                }
            }
        }
        return contador == NUMERO_TOTAL_CASILLAS;
    }

    public void vaciarTablero(){
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                casillas[i][j] = Casilla.VACIA;
            }
        }
    }
}
