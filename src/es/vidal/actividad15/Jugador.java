package es.vidal.actividad15;

public abstract class Jugador {

    private final Casilla fichaJugador;
    private final Icono icono;

    public Jugador(Casilla fichaJugador, Icono packIconos) {
        this.fichaJugador = fichaJugador;
        icono = packIconos;
    }

    public Casilla getFichaJugador() {
        return fichaJugador;
    }

    public Icono getIcono() {
        return icono;
    }

    public void ponerFicha(Tablero tablero){}

    public void cantaVictoria(){}
}