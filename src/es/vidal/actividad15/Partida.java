package es.vidal.actividad15;

import java.util.Scanner;

public class Partida {

    public static Scanner teclado = new Scanner(System.in);

    private Jugador[] jugadores;

    private Tablero tablero;

    private final Turno turno = new Turno();

    public void insertarJugadores(Jugador[] jugadoresMenu){
        jugadores = jugadoresMenu;
    }

    public void inicializarTablero(){
        tablero = new Tablero(jugadores[0].getIcono());
    }

    public Tablero getTablero() {
        return tablero;
    }

    public void jugar (){
        String seguirJugando;

        do {
            if (jugadores[0] == null){
                break;
            }
            inicializarTablero();
            jugadores[0].getIcono().seleccionar();
            juego(jugadores, tablero, turno);
            tablero.vaciarTablero();
            System.out.print("\n¿Quieres volver a jugar (S/N)? ");
            seguirJugando = teclado.nextLine();
            System.out.println();

        }while (seguirJugando.equalsIgnoreCase("S"));

        System.out.println("Hasta la próxima");
    }

    public  void juego (Jugador[] jugadores,  Tablero tablero, Turno turno){
        do {
            jugadores[turno.getActual()].ponerFicha(tablero);
            tablero.mostrar();
            if (tablero.hayTresEnRaya()) {
                jugadores[turno.getActual()].cantaVictoria();
                return;
            }
            turno.cambiar();
        }while (!tablero.estaLleno());
        System.out.println("\nLa partida ha acabado en empate");
    }
}
