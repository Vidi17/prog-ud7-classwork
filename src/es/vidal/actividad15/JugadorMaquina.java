package es.vidal.actividad15;

import java.util.Random;

public class JugadorMaquina extends Jugador{

    private final Random aleatorio = new Random();

    public JugadorMaquina(Casilla fichaJugador, Icono packIconos) {
        super(fichaJugador, packIconos);
    }

    @Override
    public void ponerFicha(Tablero tablero) {
        Coordenada coordenada;
        System.out.printf("\nTira la máquina con %s\n\n", getIcono().obtenerSimbolo(getFichaJugador()));
        retrasarRobot();
        mostrarFraseRobot();

        do {
            coordenada = obtenerCoordenadaIA(tablero);
            if(tablero.isOcupada(coordenada)){
                System.out.println("La casilla está ocupada, por favor vuelva a introducir las coordenadas");
            }
        }while (tablero.isOcupada(coordenada));
        tablero.ponerFicha(coordenada, getFichaJugador());
    }

    @Override
    public void cantaVictoria() {

        System.out.println("\nGana la partida la máquina con las fichas " +
                getIcono().obtenerSimboloJugador(getFichaJugador()));
    }

    public void mostrarFraseRobot(){
         switch (aleatorio.nextInt(5)){
            case 0 -> System.out.println("Mucho Ánimo gfe\n");
            case 1 -> System.out.println("Uff eres bueno eh\n");
            case 2 -> System.out.println("Bro Check Drip \uD83E\uDD76 \n");
            case 3 -> System.out.println("Eres to malo bro\n");
            default -> System.out.println("Nah de lokos\n");
        }
    }

    public void retrasarRobot(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Coordenada obtenerHuecoTiradaTipica(Tablero tablero, int i, int j){
        if (tablero.getCasillas()[i][j] == super.getFichaJugador() && i < 2
                && tablero.getCasillas()[i + 1][j] == Casilla.VACIA){
            return new CoordenadaIA(i + 1, j);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && i < 2 && j < 2
                && tablero.getCasillas()[i + 1][j + 1] == Casilla.VACIA){
            return new CoordenadaIA(i + 1, j + 1);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && j < 2
                && tablero.getCasillas()[i][j + 1] == Casilla.VACIA){
            return new CoordenadaIA(i, j + 1);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && i > 0
                && tablero.getCasillas()[i - 1][j] == Casilla.VACIA) {
            return new CoordenadaIA(i - 1, j);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && i > 0 && j > 0
                && tablero.getCasillas()[i - 1][j - 1] == Casilla.VACIA) {
            return new CoordenadaIA(i - 1, j - 1);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && j > 0
                && tablero.getCasillas()[i][j - 1] == Casilla.VACIA){
            return new CoordenadaIA(i , j - 1);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && j > 0 && i < 2
                && tablero.getCasillas()[i + 1][j - 1] == Casilla.VACIA){
            return new CoordenadaIA(i + 1, j - 1);
        }else if (tablero.getCasillas()[i][j] == super.getFichaJugador() && j < 2 && i > 0
                && tablero.getCasillas()[i - 1][j + 1] == Casilla.VACIA){
            return new CoordenadaIA(i - 1, j + 1);
        }else {
            return null;
        }
    }

    public Coordenada obtenerTiradaTipica(Tablero tablero){
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            for (int j = 0; j < tablero.getCasillas().length; j++) {
                if (obtenerHuecoTiradaTipica(tablero, i, j) != null){
                    return obtenerHuecoTiradaTipica(tablero, i, j);
                }
            }
        }
        return null;
    }

    public Coordenada obtenerCoordenadaIA(Tablero tablero){
        Coordenada coordenadaFila = contadorFichasFila(tablero);
        Coordenada coordenadaColumna = contadorFichasColumna(tablero);
        Coordenada coordenadaDiagonalMayor = contadorFichasDiagonalMayor(tablero);
        Coordenada coordenadaSubdiagonal = contadorFichasSubdiagonal(tablero);
        Coordenada coordenadaTiradaTipica = obtenerTiradaTipica(tablero);

        if (coordenadaFila != null){
            return coordenadaFila;
        }else if (coordenadaColumna != null) {
            return coordenadaColumna;
        }else if (coordenadaDiagonalMayor != null) {
            return coordenadaDiagonalMayor;
        }else if (coordenadaSubdiagonal != null){
            return coordenadaSubdiagonal;
        }else if (coordenadaTiradaTipica != null){
            return coordenadaTiradaTipica;
        }else {
            CoordenadaIA coordenadaIA = new CoordenadaIA();
            coordenadaIA.recoger();
            return coordenadaIA;
        }
    }

    public Coordenada contadorFichasDiagonalMayor(Tablero tablero){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            if (tablero.getCasillas()[i][i] == Casilla.FICHA_1) {
                contadorFichasX++;
            }else if (tablero.getCasillas()[i][i] == Casilla.FICHA_0){
                contadorFichas0++;
            }
            if (contadorFichasX == 2 && contadorFichas0 == 0 ){
                if (buscarHuecoDiagonalMayor(tablero) != null){
                    return buscarHuecoDiagonalMayor(tablero);
                }
            }else if (contadorFichas0 == 2 && contadorFichasX == 0){
                if (buscarHuecoDiagonalMayor(tablero) != null){
                    return buscarHuecoDiagonalMayor(tablero);
                }
            }
        }
        return null;
    }

    public Coordenada buscarHuecoDiagonalMayor(Tablero tablero){
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            if (tablero.getCasillas()[i][i] == Casilla.VACIA){
                return new CoordenadaIA(i, i);
            }
        }
        return null;
    }

    public Coordenada contadorFichasSubdiagonal(Tablero tablero){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        int j = tablero.getCasillas().length - 1;
        for (Casilla[] casilla : tablero.getCasillas()) {
            if (casilla[j] == Casilla.FICHA_1) {
                contadorFichasX++;
            } else if (casilla[j] == Casilla.FICHA_0) {
                contadorFichas0++;
            }
            if (contadorFichasX == 2 && contadorFichas0 == 0) {
                if (buscarHuecoSubdiagonal(tablero) != null) {
                    return buscarHuecoSubdiagonal(tablero);
                }
            } else if (contadorFichas0 == 2 && contadorFichasX == 0) {
                if (buscarHuecoSubdiagonal(tablero) != null) {
                    return buscarHuecoSubdiagonal(tablero);
                }
            }
            j--;
        }
        return null;
    }

    public Coordenada buscarHuecoSubdiagonal(Tablero tablero){
        int j = tablero.getCasillas().length - 1;
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            if (tablero.getCasillas()[i][j] == Casilla.VACIA){
                return new CoordenadaIA(i, j);
            }
            j--;
        }
        return null;
    }

    public Coordenada contadorFichasColumna(Tablero tablero){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            for (Casilla[] casilla : tablero.getCasillas()) {
                if (casilla[i] == Casilla.FICHA_1) {
                    contadorFichasX++;
                } else if (casilla[i] == Casilla.FICHA_0) {
                    contadorFichas0++;
                }
                if (contadorFichasX == 2 && contadorFichas0 == 0) {
                    if (buscarHuecoColumna(i, tablero) != 5) {
                        return new CoordenadaIA(buscarHuecoColumna(i, tablero), i);
                    }
                } else if (contadorFichas0 == 2 && contadorFichasX == 0) {
                    if (buscarHuecoColumna(i, tablero) != 5) {
                        return new CoordenadaIA(buscarHuecoColumna(i, tablero), i);
                    }
                }
            }
            contadorFichas0 = 0;
            contadorFichasX = 0;
        }
        return null;
    }

    public int buscarHuecoColumna(int columna, Tablero tablero){
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            if (tablero.getCasillas()[i][columna] == Casilla.VACIA){
                return i;
            }
        }
        return 5;
    }

    public Coordenada contadorFichasFila(Tablero tablero){
        int contadorFichas0 = 0;
        int contadorFichasX = 0;
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            for (int j = 0; j < tablero.getCasillas().length; j++) {
                if (tablero.getCasillas()[i][j] == Casilla.FICHA_1){
                    contadorFichasX++;
                }else if(tablero.getCasillas()[i][j] == Casilla.FICHA_0){
                    contadorFichas0++;
                }
                if (contadorFichasX == 2 && contadorFichas0 == 0){
                    if (buscarHuecoFila(i, tablero) != 5){
                        return new CoordenadaIA(i, buscarHuecoFila(i, tablero));
                    }
                }else if (contadorFichas0 == 2 && contadorFichasX == 0){
                    if (buscarHuecoFila(i, tablero) != 5){
                        return new CoordenadaIA(i, buscarHuecoFila(i, tablero));
                    }
                }
            }
            contadorFichas0 = 0;
            contadorFichasX = 0;
        }
        return null;
    }

    public int buscarHuecoFila(int fila, Tablero tablero){
        for (int i = 0; i < tablero.getCasillas().length; i++) {
            if (tablero.getCasillas()[fila][i] == Casilla.VACIA){
                return i;
            }
        }
        return 5;
    }
}
