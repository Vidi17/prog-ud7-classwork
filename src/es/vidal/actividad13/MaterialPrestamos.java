package es.vidal.actividad13;

public abstract class MaterialPrestamos {

    private final int CODIGO;

    private final String AUTOR;

    private final String TITULO;

    public MaterialPrestamos(int codigo, String autor, String titulo) {
        this.CODIGO = codigo;
        this.AUTOR = autor;
        this.TITULO = titulo;
    }

    public String getAUTOR() {
        return AUTOR;
    }

    public String getTITULO() {
        return TITULO;
    }
}
