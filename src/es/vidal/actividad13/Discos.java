package es.vidal.actividad13;

public class Discos extends MaterialPrestamos{

    private final String NOM_DISCOGRAFICA;

    public Discos(int codigo, String autor, String titulo, String nomDiscografica) {
        super(codigo, autor, titulo);
        this.NOM_DISCOGRAFICA = nomDiscografica;
    }
}
