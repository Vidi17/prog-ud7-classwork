package es.vidal.actividad13;

public class Documentos implements Imprimible{

    private final String TITULO;

    private final String FECHA_PUBLICACION;

    private final String TEXTO;

    public Documentos(String titulo, String fechaPublicacion, String texto) {
        this.TITULO = titulo;
        this.FECHA_PUBLICACION = fechaPublicacion;
        this.TEXTO = texto;
    }

    @Override
    public String obtenerTexto() {
        return "[Documento] titulo: " + TITULO + " fecha publicacion: " + FECHA_PUBLICACION + " texto: " + TEXTO;
    }
}
