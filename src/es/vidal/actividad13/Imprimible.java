package es.vidal.actividad13;

public interface Imprimible {

    String obtenerTexto();
}
