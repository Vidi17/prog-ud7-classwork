package es.vidal.actividad13;

public class Libros extends MaterialPrestamos implements Imprimible{

    private final int NUM_PAGINAS;
    private final String CAP_MUESTRA;
    private final String TITULO_CAPITULO;

    public Libros(int codIdentificativo, String titulo, String autor, int numPaginas, String capMuestra,
                  String tituloCapitulo) {
        super(codIdentificativo, titulo, autor);
        this.NUM_PAGINAS = numPaginas;
        this.CAP_MUESTRA = capMuestra;
        this.TITULO_CAPITULO = tituloCapitulo;
    }

    @Override
    public String obtenerTexto() {
        return "[LIBRO] titulo: " + super.getTITULO() + " autor: " + super.getAUTOR() + " Capitulo: título: " + TITULO_CAPITULO + " Contenido: "
                + CAP_MUESTRA;
    }
}
