package es.vidal.actividad13;

public class TestImpresora {
    public static void main(String[] args) {

        Impresora impresora = new Impresora();

        Documentos docu1 = new Documentos("Normas préstamo discos fecha 5","23-02-2020",
                "Lorem Ipsum is\nsimply dummy text of the printing and typesetting industry.");

        Documentos docu2 = new Documentos("Normas préstamo discos fecha","23-02-2020",
                "Lorem Ipsum is\nsimply dummy text of the printing and typesetting industry.");

        Libros libro1 = new Libros(2533,"La madre de Franquestein","Anónimo",234,
                "Lorem Ipsum","Capítulo introducción");

        Libros libro2 = new Libros(2533,"El heredero","Anónimo",234,
                "Lorem Ipsum","Capítulo introducción");

        impresora.imprimir(docu1);
        impresora.imprimir(docu2);
        impresora.imprimir(libro1);
        impresora.imprimir(libro2);
    }
}
