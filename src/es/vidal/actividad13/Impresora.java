package es.vidal.actividad13;

public class Impresora {
    public void imprimir(Imprimible imprimible){
        System.out.println("Imprimiendo...");
        System.out.println(imprimible.obtenerTexto());
        System.out.println();
    }
}
