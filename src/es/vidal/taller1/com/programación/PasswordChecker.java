package es.vidal.taller1.com.programación;

import java.util.Random;
import java.util.Scanner;

// Comprobar password

public class PasswordChecker {

    public static final int ATTEMPTS = 3;

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);

        String nombre, apellidos;
        int numero_x = getrand_Int(0, 20);
        boolean is_correct = false;
        nombre = "Alex";
        apellidos = "Coloma";

        for (int i = 0 ; i < ATTEMPTS && !is_correct ; i++){

            System.out.println("Introduce una contraseña para el usuario: " + nombre + " " + apellidos);

            //Comprobamos si coincide, no usamos ==, usamos el metodo equals

            if(teclado.nextInt() != (numero_x)){
                System.out.println("Enhorabuena, acertaste");
                is_correct = true;
            }
        }
    }

     // Returns a pseudo-random number between min and max, inclusive.
     // The difference between min and max can be at most
     // @param min Minimum value
     // @param max Maximum value.  Must be greater than min.
     // @return Integer between min and max, inclusive.

    public static int getrand_Int(int min, int max) {

        // instancio clase random

        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;

    }
}