package es.vidal.taller1.com.programación;


// Esta clase calcula los funciones
// seno, coseno y tangente de un angulo dado


import java.util.Scanner;

public class CalculoAngulo {

    public static void main(String[] args){

        Scanner teclado = new Scanner(System.in);

        double angulo1, angulo2;
        String nombreUsuario;


        System.out.print("Introduzca tu nombre: ");
        nombreUsuario = teclado.next();

        System.out.print("Introduzca un ángulo (0..360): ");
        angulo1 = teclado.nextFloat();
        informacion_angulo (angulo1);

        System.out.print("Introduzca un ángulo (0..360): ");
        angulo2 = teclado.nextFloat();
        informacion_angulo (angulo2);
    }

    public static void informacion_angulo (double numero){

        if(numero < 0){
            System.out.println("Solo esta permitida su " + "aplicación" + "para angulos positivos");
        }

        // como las funciones que calculan el seno, coseno y tangente trabajan en
        // radianes, hemos de pasar n de grados a radianes

        numero = Math.PI / 180 * numero;

        // otra forma sería n = Math.toRadians(n);

        System.out.println("seno: " + Math.sin(numero));
        System.out.println("coseno: " + Math.cos(numero));
        String aux = "\\u000A"; /* Unicode \n character*/
        System.out.print("tangente: " + Math.tan(numero) + aux);

    }
}