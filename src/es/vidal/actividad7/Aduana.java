package es.vidal.actividad7;

public class Aduana {

    private final Persona[] personas = new Persona[10];

    public void entrar(Persona persona) {
        if (persona instanceof PersonaInglesa && persona.casado) {
            System.out.printf("Your Welcome\n" +
                    "Hello, I'm %s, I'm %d years old and I'm married\n", persona.nombre, persona.edad);
            System.out.println("Could you show me your passport, please?");
            ((PersonaInglesa) persona).mostrarPasaporte();

        }else if (persona instanceof PersonaInglesa){
            System.out.printf("Your Welcome\n" +
                    "Hello, I'm %s, I'm %d years old and I'm not married\n", persona.nombre, persona.edad);
            System.out.println("Could you show me your passport, please?");

        }else if (persona instanceof PersonaFrancesa && persona.casado) {
            System.out.printf("Bonjour\n" +
                    "Bonjour, je suis %s, mon âge est %d et je suis marié\n", persona.nombre, persona.edad);

        }else if (persona instanceof PersonaFrancesa) {
            System.out.printf("Bonjour\n" +
                    "Bonjour, je suis %s, mon âge est %d et je ne suis marié\n", persona.nombre, persona.edad);

        }else if (persona.casado){
            System.out.printf("Bienvenido\n" +
                    "Hola soy %s, mi edad es %d y no estoy casado\n", persona.nombre, persona.edad);

        }else{
            System.out.printf("Bienvenido\n" +
                    "Hola soy %s, mi edad es %d y si estoy casado\n", persona.nombre, persona.edad);

        }
        for (int i = 0; i < personas.length; i++) {
            if (personas[i] == null){
                personas[i] = persona;
                return;
            }
        }
    }
}
