package es.vidal.actividad7;

public class TestAduana {
    public static void main(String[] args) {
        PersonaInglesa ingles1 = new PersonaInglesa("Marvin", "Murdoch", 28, true, "PCP3543123");
        PersonaFrancesa francesa1 = new PersonaFrancesa("Pierre", "Griezmann", 16, false);
        Persona persona1 = new Persona("Pepe", "Carisma Cerdà", 19, false);

        Aduana aduana = new Aduana();

        System.out.println("======== Actividad Aduana ========\n");
        aduana.entrar(ingles1);
        aduana.entrar(francesa1);
        aduana.entrar(persona1);
    }
}
