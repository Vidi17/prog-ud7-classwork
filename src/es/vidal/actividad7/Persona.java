package es.vidal.actividad7;

public class Persona {

    protected String nombre;
    protected String apellidos;
    protected int edad;
    protected boolean casado;

    public Persona(String nombre, String apellidos, int edad, boolean casado) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.casado = casado;
    }
    public Persona(String nombre, String apellidos, int edad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.casado = false;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public boolean isCasado() {
        return casado;
    }

    public void saluda(){
        String estado = (casado)? "estoy casado":"no estoy casado";
        System.out.printf("Hola soy %s, mi edad es %d y estoy %s%n",nombre,edad,estado);
    }
}
