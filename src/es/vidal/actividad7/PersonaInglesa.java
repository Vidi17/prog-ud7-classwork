package es.vidal.actividad7;

public class PersonaInglesa extends Persona {

    private final String pasaporte;

    public PersonaInglesa(String nombre, String apellidos, int edad, boolean casado, String pasaporte) {
        super(nombre, apellidos, edad, casado);
        this.pasaporte = pasaporte;
    }

    public void mostrarPasaporte(){
        System.out.printf("Mi pasaporte es: %s\n", pasaporte);
    }

    @Override
    public void saluda() {
        String estado = (casado) ? "married" : "not married";
        System.out.printf("Hello i %s, my age is %d and i %s%n", nombre, edad, estado);
    }
}
